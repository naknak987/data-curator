<?php

/**
 * Curator - Bounds Constraint
 *
 * Check that an integer is within a range.
 *
 * PHP Version 7.3.2
 *
 * @category Curate
 * @package  Data-Warehousing
 * @author   Matthew Goheen <naknak987@gmail.com>
 * @license  MIT License (see https://www.tldrlegal.com/l/mit)
 * @link     https://gitlab.com/naknak987/data-curator
 */

namespace Curator\CheckConstraints;

use Exception;

class Bounds
{
    private static $upper;
    private static $lower;

    /**
     * Upper
     *
     * Set the upper bounds.
     *
     * @param int|string $upper The value of the upper bounds.
     *
     * @return self
     */
    public static function upper($upper): self
    {
        self::$upper = $upper;
        return new static();
    }

    /**
     * Lower
     *
     * Set the lower bounds.
     *
     * @param int|string $lower The value of the lower bounds.
     *
     * @return self
     */
    public static function lower($lower): self
    {
        self::$lower = $lower;
        return new static();
    }

    /**
     * Check
     *
     * Check the given value against the upper and lower bounds.
     * Returns true if the value is on or between the upper and lower
     * bounds.
     *
     * @param int|string $value The value to check.
     *
     * @return bool
     */
    public static function check($value): bool
    {
        if (is_numeric(self::$upper)
            && is_numeric(self::$lower)
            && is_numeric($value)
        ) {
            if ($value <= self::$upper && $value >= self::$lower) {
                return true;
            }
        } else {
            $up = strcasecmp(self::$upper, $value);
            $lo = strcasecmp($value, self::$lower);

            if ($up > -1 && $lo > -1) {
                return true;
            }
        }
        return false;
    }
}
