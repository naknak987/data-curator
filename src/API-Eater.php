<?php

/**
 * Curator - API-Eater
 *
 * Provides helper methods for API requests
 *
 * PHP Version 7.3.2
 *
 * @category Curate
 * @package  Data-Curator
 * @author   Matthew Goheen <naknak987@gmail.com>
 * @license  MIT License (see https://www.tldrlegal.com/l/mit)
 * @link     https://gitlab.com/naknak987/data-curator
 */

namespace Curator;

class APIEater
{
    private $curl;
    private $response;
    private $errors;

    /**
     * Construct
     *
     * @param string $apiName The name of the API. This should correspond to the name
     *                         of the api from the .env file.
     * @param string $endPoint The endpoint for the API Call being made
     *
     * @return self
     */
    public function __construct(string $apiName, string $endPoint)
    {
        $this->curl = curl_init($_ENV[$apiName . '_API_BASE_ENDPOINT'] . $endPoint);

        $options = [
            CURLOPT_HTTPHEADER => [
                'content-type: application/json',
                'User-Api-Key: ' . $_ENV[$apiName . '_API_KEY']
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CAINFO         => __DIR__ . '/cacert.pem',
            CURLOPT_CAPATH         => __DIR__ . '/cacert.pem',
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_CUSTOMREQUEST  => 'GET'
        ];

        curl_setopt_array($this->curl, $options);

        return $this;
    }

    /**
     * Get
     *
     * @return bool
     */
    public function get(): bool
    {
        $this->response = curl_exec($this->curl);

        $this->errors = curl_error($this->curl);

        if ($this->errors != '') {
            return false;
        }

        return true;
    }

    /**
     * Get Results
     *
     * Returns the results of the last curl request that was executed.
     *
     * @return array
     */
    public function getResults(): array
    {
        return json_decode($this->response, true);
    }

    /**
     * Get Errors
     *
     * Returns the errors from the last curl request that was executed.
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
