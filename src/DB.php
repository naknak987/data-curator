<?php

/**
 * Curator - DB
 *
 * Provides the DB class to curator commands. A lightweight
 * database query layer.
 *
 * PHP Version 7.3.2
 *
 * @category Curate
 * @package  Data-Warehousing
 * @author   Matthew Goheen <naknak987@gmail.com>
 * @license  MIT License (see https://www.tldrlegal.com/l/mit)
 * @link     https://gitlab.com/naknak987/data-curator
 */

namespace Curator;

use PDO;
use Exception;
use PDOException;

class DB
{
    private static $wh;
    private static $currentWh;
    private static $qTable;
    private static $qJoins;
    private static $qSelect;
    private static $qWhere;
    private static $query;
    private static $qRaw = false;
    private static $parameters = [
        'count' => 0,
        'list' => []
    ];
    private static $dataTypes = [];

    /**
     * Reset the private properties of this class
     */
    private static function reset()
    {
        //self::$wh = '';
        self::$qTable = null;
        self::$qJoins = null;
        self::$qSelect = null;
        self::$qWhere = null;
        self::$query = null;
        self::$qRaw = false;
        self::$parameters = [
            'count' => 0,
            'list' => []
        ];
    }

    /**
     * Warehouse - Connect to a warehouse/database.
     */
    public static function warehouse(string $warehouse = 'DEFAULT')
    {
        $db = $_ENV[$warehouse . '_WH_DB'];
        $orig_warehouse = $warehouse;

        if (isset($_ENV[$warehouse . '_WH_USE_DEFAULT'])
        && $_ENV[$warehouse . '_WH_USE_DEFAULT'] == true) {
            $warehouse = 'DEFAULT';
        }

        if (!isset(self::$wh[$orig_warehouse])) {
            if ($_ENV[$warehouse . '_WH_DRIVER'] == 'mysql') {
                $dsn = "mysql:host={$_ENV[$warehouse.'_WH']};dbname={$db};charset=utf8mb4";
                $options = [
                    PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES      => false,
                ];
            } elseif ($_ENV[$warehouse . '_WH_DRIVER'] == 'sqlsrv') {
                $dsn = "sqlsrv:Server={$_ENV[$warehouse.'_WH']};Database={$db}";
                $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,];
            } else {
                throw new Exception('Incompatible or no database driver specified.');
            }

            try {
                self::$wh[$orig_warehouse] = new PDO(
                    $dsn,
                    $_ENV[$warehouse . '_WH_USER'],
                    $_ENV[$warehouse . '_WH_PASS'],
                    $options
                );
            } catch (\PDOException $e) {
                throw new PDOException($e->getMessage(), (int)$e->getCode());
            }
        }

        self::$currentWh = $orig_warehouse;

        return new static();
    }

    /**
     * Table - Set the table we will be using for the query.
     */
    public static function table(string $table)
    {
        self::$qTable = $table;
        return new static();
    }

    /**
     * Create - Create a table in the database
     */
    public static function create(array $columns)
    {
        $col_cnt = (count($columns) - 1);

        $sql = 'CREATE TABLE IF NOT EXISTS ' . $_ENV[self::$currentWh.'_WH_DB'] . '.' . self::$qTable . ' (';
        foreach ($columns as $index => $name) {
            $sql .= '`' . $name . '` ' . self::$dataTypes[$index];
            if ($col_cnt !== $index) {
                $sql .= ', ';
            }
        }
        $sql .= ')';

        $stmt = self::$wh[self::$currentWh]->prepare($sql);

        if (!$stmt->execute(self::$parameters['list'])) {
            self::throwExecError($stmt->errorInfo());
        }

        self::reset();
    }

    /**
     * Detect Data Types
     */
    public static function detectDataTypes(array $row)
    {
        // Reset data type array first to support loading more than one CSV in one script.
        self::$dataTypes = [];

        foreach($row as $val) {
            //types conversions and redetection needs more work. messes up on inserting "" into decimal columns.
            /* if (is_numeric($val)) {
                self::$dataTypes[] = 'INT';
            } else { */
                $length = strlen($val);
                if ($length > 255) {
                    self::$dataTypes[] = 'TEXT';
                } else {
                    self::$dataTypes[] = "VARCHAR({$length})";
                }
            //}
        }
        return new static();
    }

    /**
     * Redetect Data Types
     */
    public static function redetectDataTypes(array $row)
    {
        $dataTypeChanged = false;
        $cnt = 0;
        $newColumnDefinitions = [];
        foreach($row as $col_name => $val) {
            if ($val != ''
            && !is_numeric($val)
            && self::$dataTypes[$cnt] == 'INT'
            ) {
                $length = strlen($val);
                if ($length > 255) {
                    self::$dataTypes[$cnt] = 'TEXT';
                    $dataTypeChanged = true;
                } else {
                    self::$dataTypes[$cnt] = "VARCHAR({$length})";
                    $dataTypeChanged = true;
                }
            } elseif ($val != ''
            && is_numeric($val)
            && (self::$dataTypes[$cnt] == 'INT' || strpos(self::$dataTypes[$cnt], 'DECIMAL') !== FALSE)
            && (strpos($val, '.') !== false)
            ) {
                $exp_val = explode('.', $val);
                $percision = strlen($val) - 1;
                $scale = strlen($exp_val[1]);

                if (strpos(self::$dataTypes[$cnt], 'DECIMAL') !== FALSE) {
                    $percision_scale = str_replace('DECIMAL(', '', self::$dataTypes[$cnt]);
                    $percision_scale = str_replace(') DEFAULT 0', '', $percision_scale);

                    $percision_scale = explode(',', $percision_scale);

                    if ($percision > $percision_scale[0]) {
                        self::$dataTypes[$cnt] = str_replace($percision_scale[0], $percision, self::$dataTypes[$cnt]);
                        $dataTypeChanged = true;
                    }

                    if ($scale > $percision_scale[1]) {
                        self::$dataTypes[$cnt] = str_replace($percision_scale[1], $scale, self::$dataTypes[$cnt]);
                        $dataTypeChanged = true;
                    }

                } else {
                    self::$dataTypes[$cnt] = 'DECIMAL(' . $percision . ',' . $scale . ') DEFAULT 0';
                    $dataTypeChanged = true;
                }
            } elseif ($val != '') {
                $length = strlen($val);
                if (strpos(self::$dataTypes[$cnt], 'VARCHAR') !== false) {
                    if ($length > 255) {
                        self::$dataTypes[$cnt] = 'TEXT';
                        $dataTypeChanged = true;
                    } else {
                        $old_length = str_replace('VARCHAR(', '', self::$dataTypes[$cnt]);
                        $old_length = str_replace(')', '', $old_length);

                        if ($length > $old_length) {
                            self::$dataTypes[$cnt] = "VARCHAR({$length})";
                            $dataTypeChanged = true;
                        }
                    }
                }
            }

            if ($dataTypeChanged) {
                $sql = 'ALTER TABLE IF EXISTS `' . $_ENV[self::$currentWh.'_WH_DB'] . '`.`' . self::$qTable . '` MODIFY COLUMN IF EXISTS ';
                $sql .= '`' . $col_name . '` ' . self::$dataTypes[$cnt] ;

                $stmt = self::$wh[self::$currentWh]->prepare($sql);

                if (!$stmt->execute(self::$parameters['list'])) {
                    self::throwExecError($stmt->errorInfo());
                }

                $dataTypeChanged = false;
            }

            $cnt++;
        }


    }

    /**
     * Join
     *
     * Join a new table on a previous table.
     *
     * @param string $newTable        The name of the new table we are joining.
     * @param string $joinTableColumn The table and column name that we are joining on.
     *                                Expressed as 'table.column'.
     * @param string $newTableColumn  The table and column name thats being joined.
     *                                Expressed as 'table.column'.
     * @param string $joinOperator    The operator to join with. Default if '='.
     *
     * @return self
     */
    public static function join(
        string $newTable,
        string $joinTableColumn,
        string $newTableColumn,
        string $joinOperator = '='
    ) {
        $tQ = " JOIN $newTable ON $joinTableColumn $joinOperator $newTableColumn";
        self::$qJoins[] = $tQ;

        return new static();
    }

    /**
     * Left Join
     *
     * @param string $newTable
     * @param string $joinTableColumn
     * @param string $newTableColumn
     * @param string $joinOperator
     *
     * @return self
     */
    public static function leftJoin(
        string $newTable,
        string $joinTableColumn,
        string $newTableColumn,
        string $joinOperator = '='
    ): self {
        $tQ = " LEFT JOIN $newTable ON $joinTableColumn $joinOperator $newTableColumn";
        self::$qJoins[] = $tQ;

        return new static();
    }

    /**
     * Select - Prepares a select statement for the specified table.
     *
     * @param string|array $columns  Can either be a string to select a single column
     *                               or an array to select multiple columns.
     * @param bool         $distinct Select distinct values for each column.
     *
     * @return self
     * @throws Exception
     */
    public static function select($columns = '*', $distinct = false)
    {
        $tQ = "SELECT";
        if ($distinct) {
            $tQ = $tQ . ' DISTINCT';
        }
        if (is_string($columns)) {
            $tQ = "$tQ $columns";
        } elseif (is_array($columns)) {
            for ($i = 0; $i < count($columns); $i++) {
                if ($i != (count($columns) - 1)) {
                    $tQ = "$tQ $columns[$i],";
                } else {
                    $tQ = "$tQ $columns[$i]";
                }
            }
        } else {
            throw new Exception('columns must be a string or an array');
        }

        self::$qSelect = $tQ;

        return new static();
    }

    /**
     * Where
     *
     * Build a where clause for the query.
     *
     * @param string $lCondition The left hand side of the condition.
     * @param string $operator   The comparision operator to use. The comparision operator can be any of
     *                           ('=', '>', '<', '>=', '<=', '<>', '!=', '!<', '!>').
     * @param string $rCondition The right hand side of the condition.
     *
     * @return self
     */
    public static function where($lCondition, $operator = '', $rCondition = '')
    {
        if (self::$qWhere == null) {
            $tQ = ' WHERE';
        } else {
            $tQ = '';
        }

        if (is_string($lCondition) && $operator != '' && $rCondition != '') {
            self::$parameters['list'][':parameter' . self::$parameters['count']] = $rCondition;
            $tQ = "$tQ $lCondition $operator :parameter" . self::$parameters['count']++ . " ";
        } elseif (is_string($lCondition) && $operator != '' && $rCondition == null) {
            if ($operator == '=') {
                $tQ = "$tQ $lCondition IS NULL ";
            } elseif ($operator == '!=' || $operator == '<>') {
                $tQ = "$tQ $lCondition IS NOT NULL ";
            }
        } else {
            throw new Exception('provided parameters incorrect');
        }

        if (self::$qWhere == null) {
            self::$qWhere = $tQ;
        } else {
            self::$qWhere = self::$qWhere . $tQ;
        }

        return new static();
    }

    /**
     * And Where
     *
     * Add on to a where clause for the query. Must use 'where()' first.
     *
     * @param string $lCondition The left hand side of the condition.
     * @param string $operator   The comparision operator to use. The comparision operator can be any of
     *                           ('=', '>', '<', '>=', '<=', '<>', '!=', '!<', '!>').
     * @param string $rCondition The right hand side of the condition.
     *
     * @return self
     * @throws Exception
     */
    public static function andWhere($lCondition, $operator = '', $rCondition = '')
    {
        $tQ = ' AND';
        if (is_string($lCondition) && $operator != '' && $rCondition != '') {
            self::$parameters['list'][':parameter' . self::$parameters['count']] = $rCondition;
            $tQ = "$tQ $lCondition $operator :parameter" . self::$parameters['count']++ . " ";
        } elseif (is_string($lCondition) && $operator != '' && $rCondition == null) {
            if ($operator == '=') {
                $tQ = "$tQ $lCondition IS NULL ";
            } elseif ($operator == '!=' || $operator == '<>') {
                $tQ = "$tQ $lCondition IS NOT NULL ";
            }
        } else {
            throw new Exception('provided parameters incorrect');
        }

        self::$qWhere = self::$qWhere . $tQ;

        return new static();
    }

    /**
     * And
     *
     * @return self
     */
    public static function and(): self
    {
        if (self::$qWhere != null) {
            self::$qWhere = self::$qWhere . ' AND ';
        }

        return new static();
    }

    /**
     * Or
     *
     * @return self
     */
    public static function or(): self
    {
        if (self::$qWhere != null) {
            self::$qWhere = self::$qWhere . ' OR ';
        }

        return new static();
    }

    /**
     * Where In
     *
     * Add a WHERE  clause to an existing query in order to exclude certain
     * values from the results of the query.
     *
     * @param string $columnName The name of the column to look in.
     * @param array  $values     An array of values to exclude from the results.
     *
     * @return self
     * @throws Exception
     */
    public static function whereIn(string $columnName, array $values)
    {
        if (self::$qWhere == null) {
            $tQ = " WHERE $columnName IN (";
        } else {
            $tQ = self::$qWhere . 'AND `' . $columnName . '` IN (';
        }

        $pList = [];

        foreach ($values as $val) {
            self::$parameters['list'][':parameter' . self::$parameters['count']] = $val;
            $pList[] = ':parameter' . self::$parameters['count']++;
        }

        $tQ = $tQ . implode(',', $pList);

        $tQ = $tQ . ') ';
        self::$qWhere = $tQ;

        return new static();
    }

    /**
     * Where Not In
     *
     * Add a Not In Where clause to an existing query in order to exclude certain
     * values from the results of the query.
     *
     * @param string $columnName The name of the column to look in.
     * @param array  $values     An array of values to exclude from the results.
     *
     * @return self
     * @throws Exception
     */
    public static function whereNotIn(string $columnName, array $values)
    {
        if (self::$qWhere == null) {
            $tQ = " WHERE `$columnName` NOT IN (";
        } else {
            $tQ = self::$qWhere . 'AND `' . $columnName . '` NOT IN (';
        }

        foreach ($values as $val) {
            self::$parameters['list'][':parameter' . self::$parameters['count']] = $val;
            if ($val == $values[count($values) - 1]) {
                $tQ = $tQ . ':parameter' . self::$parameters['count']++;
            } else {
                $tQ = $tQ . ':parameter' . self::$parameters['count']++ . ',';
            }
        }

        $tQ = $tQ . ') ';
        self::$qWhere = $tQ;

        return new static();
    }

    /**
     * Where Between
     *
     * @param string $col The column that must be between A and B.
     * @param string $a   The beginning of the range that $col must be between.
     * @param string $b   The ending of the range that $col must be between.
     *
     * @return self
     */
    public static function whereBetween(
        string $col,
        string $a,
        string $b
    ): self {
        if (self::$qWhere == null) {
            $tQ = " WHERE $col BETWEEN ";
        } else {
            $tQ = self::$qWhere . " AND $col BETWEEN ";
        }

        self::$parameters['list'][':parameter' . self::$parameters['count']] = $a;
        $tQ = $tQ . ':parameter' . self::$parameters['count']++ . ' AND ';

        self::$parameters['list'][':parameter' . self::$parameters['count']] = $b;
        $tQ = $tQ . ':parameter' . self::$parameters['count']++ . ' ';

        self::$qWhere = $tQ;

        return new static();
    }

    /**
     * Get - Execute the query on the connected database/warehouse.
     */
    public static function get($order = null)
    {
        if (!self::$qRaw) {
            if (self::$qSelect !== null) {
                self::$query = self::$query . self::$qSelect;
            } else {
                self::$query = self::$query . '*';
            }
            if (self::$qTable !== null) {
                self::$query = self::$query . ' FROM ' . self::$qTable;
            } else {
                throw new Exception('No table was specified. You must call table() to specify the table name.');
            }
            if (self::$qJoins !== null) {
                foreach (self::$qJoins as $join) {
                    self::$query = self::$query . $join;
                }
            }
            if (self::$qWhere !== null) {
                self::$query = self::$query . self::$qWhere;
            }
        }

        if (self::$query === null) {
            throw new Exception('NULL QUERY: Cannot execute empty queries.');
        }

        if ($order !== null) {
            self::$query = self::$query . ' order by ' . $order;
        }

        $stmt = self::$wh[self::$currentWh]->prepare(self::$query);

        if (!$stmt->execute(self::$parameters['list'])) {
            self::throwExecError($stmt->errorInfo());
        }

        self::reset();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Insert - Insert data into the specified table on the connected database/warehouse.
     *
     * @param array $array An associative array of the values that should be inserted.
     *                     The array keys should be the column names.
     *
     * @param bool  $getID Whether or not the id of the inserted column should be returned.
     *
     * @return bool|int
     */
    public static function insert(array $array, $getID = false)
    {

        if (is_array($array)) {
            $queryString = 'INSERT INTO ' . self::$qTable;

            $columns = implode(', ', array_keys($array));
            $queryString = $queryString . " ($columns) VALUES ";

            foreach ($array as $val) {
                self::$parameters['list'][':parameter' . self::$parameters['count']++ ] = $val;
            }

            $values = implode(', ', array_keys(self::$parameters['list']));
            $queryString = "$queryString($values);";

            $stmt = self::$wh[self::$currentWh]->prepare($queryString);
            if (!$stmt->execute(self::$parameters['list'])) {
                self::throwExecError($stmt->errorInfo());
            }

            $id = self::$wh[self::$currentWh]->lastInsertId();
            self::reset();

            if ($getID) {
                return $id;
            } else {
                return true;
            }
        } else {
            throw new Exception('non-array given');
        }
    }

    /**
     * Insert On New Table - Insert data into a newly created table on the connected database/warehouse
     * adjusting data types as needed.
     *
     * @param array $array An associative array of the values that should be inserted.
     *                     The array keys should be the column names.
     *
     * @param bool  $getID Whether or not the id of the inserted column should be returned.
     *
     * @return bool|int
     */
    public static function insertOnNewTable(array $array, $getID = false)
    {
        if (empty(self::$dataTypes)) {
            throw new Exception('Data types array is empty. You must create the table first.');
        }

        self::redetectDataTypes($array);

        if (is_array($array)) {
            $queryString = 'INSERT INTO ' . self::$qTable;

            $columns = implode('`, `', array_keys($array));
            $queryString = $queryString . " (`$columns`) VALUES ";

            foreach ($array as $val) {
                self::$parameters['list'][':parameter' . self::$parameters['count']++ ] = $val;
            }

            $values = implode(', ', array_keys(self::$parameters['list']));
            $queryString = "$queryString($values);";

            $stmt = self::$wh[self::$currentWh]->prepare($queryString);
            if (!$stmt->execute(self::$parameters['list'])) {
                self::throwExecError($stmt->errorInfo());
            }

            $id = self::$wh[self::$currentWh]->lastInsertId();
            self::reset();

            if ($getID) {
                return $id;
            } else {
                return true;
            }
        } else {
            throw new Exception('non-array given');
        }
    }

    /**
     * Update
     *
     * Preparing and execute an update statement. Must run 'DB::where()' first.
     *
     * @param string|array $set The name of the column we want to update.
     * @param string       $to  The new value that the column should be.
     *
     * @return bool
     */
    public static function update($set, $to = null)
    {
        if (isset(self::$qWhere) && self::$qWhere != '') {
            if (is_array($set)) {
                $tQ = 'UPDATE ' . self::$qTable . ' SET ';
                $fLoop = true;
                foreach ($set as $col => $val) {
                    if ($fLoop) {
                        $fLoop = false;
                    } else {
                        $tQ = $tQ . ', ';
                    }
                    self::$parameters['list'][':parameter' . self::$parameters['count']] = $val;
                    $tQ = $tQ . $col . ' = :parameter' . self::$parameters['count']++;
                }
                $tQ = $tQ . ' ' . self::$qWhere;
            } elseif (is_string($set) && is_string($to)) {
                self::$parameters['list'][':parameter' . self::$parameters['count']] = $to;

                $tQ = 'UPDATE ' .
                    self::$qTable .
                    ' SET ' .
                    $set .
                    ' = :parameter' .
                    self::$parameters['count']++ .
                    ' ' .
                    self::$qWhere;
            } else {
                throw new Exception(
                    'First parameter must be an array or a string. ' .
                    'If first parameter is a string then the second paramter must also be a string.'
                );
            }

            $stmt = self::$wh[self::$currentWh]->prepare($tQ);
            if (!$stmt->execute(self::$parameters['list'])) {
                self::throwExecError($stmt->errorInfo());
            }

            self::reset();

            if ($stmt->rowCount() >= 1) {
                return true;
            } else {
                return false; // No rows affected.
            }
        }
        throw new Exception('where() must be called before calling update()');
    }

    /**
     * Delete
     *
     * Prepare and execute a DELETE statement. Must call 'DB::where()' first.
     *
     * @return bool
     */
    public static function delete(): bool
    {
        $tQ = 'DELETE FROM ' . self::$qTable . ' ' . self::$qWhere;

        $stmt = self::$wh[self::$currentWh]->prepare($tQ);
        if (!$stmt->execute(self::$parameters['list'])) {
            self::throwExecError($stmt->errorInfo);
        }

        self::reset();

        if ($stmt->rowCount() >= 1) {
            return true;
        } else {
            return false; // No rows deleted.
        }
    }

    /**
     * Truncate
     *
     * Truncates a table from a database. (Removes all data in table)
     *
     * @return void
     */
    public static function truncate(): void
    {
        $tQ = 'TRUNCATE ' . self::$qTable;

        $stmt = self::$wh[self::$currentWh]->prepare($tQ);
        if (!$stmt->execute(self::$parameters['list'])) {
            self::throwExecError($stmt->errorInfo);
        }

        self::reset();
    }

    /**
     * Raw - Prepares a raw query string for execution.
     *
     * @param string $query      The query string to prepare.
     * @param array  $parameters Array of parameters for the query. Must use key value
     *                           pairs where the key is pdo parameter marker.
     *                           (Ex. $arr = [':param' => 'value'])
     *
     * @throws Exception
     * @return self
     */
    public static function raw(string $query, array $parameters = null): self
    {
        self::$qRaw = true;
        self::$query = $query;

        if ($parameters != null) {
            foreach ($parameters as $l => $p) {
                if (strpos($l, ':') === 0 && strlen($l) > 1) {
                    self::$parameters['list'][$l] = $p;
                    self::$parameters['count'] += 1;
                } else {
                    throw new Exception(
                        <<<ERR
The key for parameter '${p}' must have a valid parameter marker.
The given marker, '${l}', does not appear to be valid.
Parameter markers must have a colon ":" and at least one other character.
(Ex. ':pMarker')
ERR
                    );
                }

            }
        }

        return new static();
    }

    /**
     * Throw Exec Error
     *
     * Throws an error using the results of SQLStatement::errorInfo()
     *
     * @param array $error The array that SQLStatement::errorInfo() returns
     *
     * @return void
     */
    public static function throwExecError(array $error)
    {
        self::reset();
        throw new Exception(
            "SQL State: '{$error[0]}' \r\n" .
            "Error Code: '{$error[1]}' \r\n" .
            "Message: '{$error[2]}'"
        );
    }

    /**
     * Exists
     *
     * simple check to see if the table exists. warehouse() and table()
     * must be called first.
     *
     * @return bool
     */
    public static function exists(): bool
    {
        $tbl = self::$qTable;
        $scm = $_ENV[self::$currentWh . '_WH_DB'];

        $tQ = <<<SQL
        select *
        from information_schema.tables
        where table_schema = '{$scm}'
        and table_name = '{$tbl}'
SQL;
        $stmt = self::$wh[self::$currentWh]->prepare($tQ);
        if (!$stmt->execute(self::$parameters['list'])) {
            self::throwExecError($stmt->errorInfo);
        }

        self::reset();

        if ($stmt->rowCount() >= 1) {
            return true;
        } else {
            return false; // No rows deleted.
        }
    }
}
